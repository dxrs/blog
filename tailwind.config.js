/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,jsx,ts,tsx}',
    './src/components/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    fontFamily: {
      heading: ['"Merriweather Sans"', 'sans-serif'],
    },
    extend: {
      colors: {
        'jet-dark': '#2A2B2A',
        'black-shadow': '#C9B1BD',
      },
    },
  },
  plugins: [],
}
