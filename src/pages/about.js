import * as React from 'react'
import Layout from '../components/layout'
import Seo from '../components/seo'

const AboutPage = () => {
  return (
    <Layout pageTitle="Tentang">
      <p>This is about me</p>
    </Layout>
  )
}

export const Head = () => <Seo title="Tentang" />

export default AboutPage
