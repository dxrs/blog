import * as React from 'react'
import { Link, graphql } from 'gatsby'
import Layout from '../components/layout'

const IndexPage = ({ data }) => {
  return (
    <div class="min-h-screen w-full bg-jet-dark text-white">
      <Layout pageTitle="Beranda">
        {data.allMdx.nodes.map((node) => (
          <article class="flex-row mb-5" key={node.id}>
            <h2 class="text-black-shadow text-2xl">
              <Link to={`/${node.frontmatter.slug}`}>
                {node.frontmatter.title}
              </Link>
            </h2>
            <p class="text-xs my-1">{node.frontmatter.date}</p>
            <p>{node.excerpt}</p>
          </article>
        ))}
      </Layout>
    </div>
  )
}

export const query = graphql`
  query {
    allMdx(sort: { frontmatter: { date: DESC } }) {
      nodes {
        frontmatter {
          date(formatString: "MMM D, YYYY")
          title
          slug
        }
        id
        excerpt
      }
    }
    site {
      siteMetadata {
        title
      }
    }
  }
`

export const Head = ({ data }) => (
  <title>Beranda — {data.site.siteMetadata.title}</title>
)

export default IndexPage
