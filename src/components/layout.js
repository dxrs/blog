import * as React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'

const Layout = ({ pageTitle, children }) => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <div class="container max-w-2xl mx-auto p-2">
      <nav class="flex justify-between items-center h-12 mb-12">
        <header class="text-2xl text-black-shadow font-extrabold py-4">
          <Link to="/">{data.site.siteMetadata.title}</Link>
        </header>
      </nav>
      <main>{children}</main>
    </div>
  )
}

export default Layout
